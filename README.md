# Défi: MLOps pour la Détection des Débuts de Crises Épileptiques

## Description du Défi

Le défi consiste à développer un modèle de détection du début des crises épileptiques en se basant sur l'analyse de signaux physiologiques. Les participants sont appelés à utiliser les meilleures pratiques de MLOps pour développer, déployer et gérer leur modèle de manière efficace et reproductible. La solution proposée devra inclure un processus d'extraction de features à partir des signaux physiologiques, permettant de capturer des informations pertinentes pour la détection des crises. Le modèle devra être capable de classifier les états épileptiques afin de détecter le début des crises avec une précision élevée, tout en minimisant les faux positifs et les faux négatifs. En outre, il est demandé de concevoir une interface utilisateur permettant aux médecins de visualiser les détections.

[Lien original de  la base CHB-MIT Scalp EEG Database.](https://physionet.org/content/chbmit/1.0.0/chb01/#files-panel)

## Description du Pipeline

Pour créer un pipeline de manière efficace et reproductible, les participants doivent utiliser ZenML. Voici les étapes du pipeline:

1. **Initialisation du dépôt ZenML :** Initialisation du référentiel ZenML pour gérer le code et les artefacts du pipeline.
   
2. **Définition de la source de données CSV :** Définition de la source de données pour lire les ensembles de données CSV, telles que la base de données CHB-MIT Scalp EEG déjà préparée.
   
3. **Fractionnement des données :** Division des données en ensembles de formation et de validation pour l'évaluation du modèle.
   
4. **Construction du pipeline ZenML :** Création du pipeline à l'aide de ZenML pour le traitement des données et la construction de modèles.
   
5. **Formation en pipeline :** Entraînement du pipeline pour entraîner le modèle à l’aide de données préparées.
   
6. **Évaluation du modèle :** Validation des performances du modèle sur l’ensemble par rapport à une baseline établie.
   
7. **Exportation du modèle :** Exportation du modèle entraîné pour un déploiement ultérieur sur une interface utilisateur pour la visualisation des détections.

N.B. MLFlow peut également être utilisé avec ou à la place de ZenML. Voici les liens vers les documentations :
- [Documentation de ZenML](https://docs.zenml.io/)
- [Documentation de MLFlow](https://mlflow.org/docs/latest/index.html)

## Description du Dataset

La classification des crises d'épilepsie est une tâche cruciale dans le domaine médical, notamment dans le diagnostic et la prise en charge des patients atteints d'épilepsie. Dans le contexte de l'électroencéphalographie (EEG), la classification des crises d'épilepsie vise à identifier les périodes de l'activité électrique cérébrale qui correspondent à une crise épileptique par rapport aux périodes d'activité électrique normale.

Le CHB-MIT Scalp EEG Database est une ressource précieuse dans le domaine de la recherche sur l'épilepsie. Il s'agit d'une collection d'enregistrements EEG de sujets pédiatriques présentant des crises épileptiques réfractaires. Ces enregistrements ont été effectués dans le cadre d'une surveillance prolongée après l'arrêt de la médication antiépileptique, dans le but de caractériser les crises et d'évaluer l'éligibilité des patients à une intervention chirurgicale. Au total, 182 crises ont été annotées, fournissant une base de données riche en données pour étudier les modèles d'activité cérébrale associés aux crises épileptiques chez les enfants.

Le processus de classification peut être réalisé en utilisant des techniques d'apprentissage automatique, où les caractéristiques extraites des signaux EEG sont utilisées pour distinguer entre les états de crise épileptique et les états normaux.

Les états peuvent être étiquetés comme suit :
- 0 (EEG interictal, cerveau normal) : Cette étiquette est associée aux périodes où l'EEG enregistre une activité électrique cérébrale normale, en dehors des périodes de crises épileptiques. Pendant ces périodes, le patient ne présente pas de symptômes d'épilepsie et son cerveau fonctionne dans des limites normales.
- 1 (Crise d'épilepsie) : Cette étiquette est associée aux périodes où l'EEG enregistre une activité électrique cérébrale caractéristique des crises épileptiques. Ces périodes sont marquées par des modèles d'activité électrique anormaux, tels que des décharges épileptiformes, des pointes ou des ondes lentes.

La classification des crises d'épilepsie a plusieurs implications cliniques importantes :
- Diagnostic précoce et précis
- Suivi de l'évolution de la maladie
- Optimisation du traitement
- Prévention des crises

Le processus d'extraction de caractéristiques revêt une importance cruciale dans la classification des crises d'épilepsie à partir des enregistrements EEG, permettant la réduction de la dimensionnalité des données, la capture de motifs caractéristiques pertinents, l'amélioration de la séparabilité des classes, la robustesse aux artefacts et au bruit, ainsi que la facilitation de l'interprétation des résultats pour mieux comprendre la pathophysiologie de l'épilepsie.

Les participants doivent utiliser les datasets disponibles dans le GitLab du défi pour mener à bien leur travail.

Voici une Description textuelle de chaque caractéristique dans Dataset :
1. DFA (Detrended Fluctuation Analysis) : Analyse des fluctuations détrendues de la série temporelle.
2. Fisher Information : Mesure de l'information de Fisher, une quantité liée à la capacité d'un processus stochastique à transporter des informations sur un paramètre inconnu.
3. HFD (Higuchi Fractal Dimension) : Dimension fractale de Higuchi, une mesure de la complexité fractale d'une série temporelle.
4. PFD (Petrosian Fractal Dimension) : Dimension fractale de Petrosian, une mesure de la rugosité ou de la complexité fractale d'une série temporelle.
5. SVD (Singular Value Decomposition) Entropy : Entropie de la décomposition en valeurs singulières, une mesure de la complexité de la série temporelle basée sur la décomposition en valeurs singulières.
6. Variance : Variance de la série temporelle, mesurant la dispersion des valeurs par rapport à la moyenne.
7. Écart type : Mesure de la dispersion des valeurs par rapport à la moyenne, similaire à la variance mais en unités de la série temporelle.
8. Moyenne : Moyenne arithmétique de la série temporelle, représentant la tendance centrale des données.
9. Variance de la transformée de Fourier (FFT) : Variance des coefficients de la transformée de Fourier rapide (FFT), mesurant la dispersion des fréquences présentes dans le signal.
10. Écart type de la transformée de Fourier (FFT) : Mesure de la dispersion des coefficients de la transformée de Fourier rapide (FFT) par rapport à leur moyenne.
11. Variance de la deuxième transformée de Fourier (FFT2) : Variance des coefficients de la deuxième transformée de Fourier, fournissant des informations supplémentaires sur les fréquences présentes dans le signal.
12. Taux de passage par zéro : Nombre de passages à travers zéro dans la série temporelle, indiquant les changements de direction dans le signal.
13. Complexité : Mesure de la complexité du signal, basée sur des critères tels que l'irrégularité et la variabilité.

--- 

## Edit: Clarification des canaux

- Ch0x fait référence au nom du patient.
- Le channel (canal) 1 ou 2 est la source du signal EEG.

Dans les enregistrements EEG, chaque canal représente un emplacement spécifique d'électrode sur le cuir chevelu. La convention de dénomination des canaux suit généralement un système standardisé, tel que le système 10-20, où la désignation « Ch0x » fait référence au nom du patient et « Canal 1 » ou « Canal 2 » correspond à la source du signal EEG. enregistré à partir des électrodes respectives. Ces canaux fournissent des informations spatiales sur l’activité cérébrale, permettant aux chercheurs d’analyser les signaux électriques provenant de différentes régions du cerveau.